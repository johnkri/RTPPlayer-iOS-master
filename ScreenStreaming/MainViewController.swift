//
//  MainViewController.swift
//  ScreenStreaming
//
//  Created by John Kricorian on 25/02/2022.
//

import UIKit
import ReplayKit


protocol MainViewDelegate: AnyObject {
    var customRecorder: CustomRecorderDelegate? { get set }
    var enqueueur: EnqueuerDelegate? { get set }
}

class MainViewController: UIViewController, MainViewDelegate {

    private var timer = Timer()
    private var recorder = RPScreenRecorder.shared()
    var customRecorder: CustomRecorderDelegate?
    var enqueueur: EnqueuerDelegate?
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .landscapeLeft
    }
    
    override var shouldAutorotate: Bool {
        return true
    }
        
    override func viewDidLoad() {
        super.viewDidLoad()
        UIDevice.current.setValue(UIInterfaceOrientation.landscapeRight.rawValue, forKey: "orientation")
//        timer = Timer.scheduledTimer(withTimeInterval: 1, repeats: true, block: { _ in
//            self.updateContainerBackgroundColor()
//        })
        customRecorder?.startRecord()
//        startRecord()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        let secondViewController = SecondViewController()
//        present(secondViewController, animated: true)
    }

    private func startRecord() {
        recorder.startCapture { sampleBuffer, type, error in
            self.enqueueur?.sampleBuffer = sampleBuffer
        }
    }
    
    private func updateContainerBackgroundColor() {
        view.backgroundColor = UIColor.random()
    }
}
