//
//  NSLocking+Extension.swift
//  RTPPlayer
//
//  Created by John Kricorian on 14/02/2022.
//  Copyright © 2022 Smartbox. All rights reserved.
//

import Foundation

extension NSLocking {
    func sync<T>(_ closure: () throws -> T) rethrows -> T {
        lock()
        defer { unlock() }
        return try closure()
    }
}
