//
//  File.swift
//  VideoStream
//
//  Created by John Kricorian on 01.01.22.
//  Copyright © 2022 John Kricorian All rights reserved.
//

class ByteUtil {
    
    static func bytesToUInt16(_ bytes: ArraySlice<UInt8>) -> UInt16 {
        
        let bigEndianValue = bytes.withUnsafeBufferPointer {
            ($0.baseAddress!.withMemoryRebound(to: UInt16.self, capacity: 1) { $0 })
            }.pointee
        
        return UInt16(bigEndian: bigEndianValue)
        
    }
    
    static func bytesToUInt32 (_ bytes: ArraySlice<UInt8>) -> UInt32 {
        
        let bigEndianValue = bytes.withUnsafeBufferPointer {
            ($0.baseAddress!.withMemoryRebound(to: UInt32.self, capacity: 1) { $0 })
            }.pointee
        
        return UInt32(bigEndian: bigEndianValue)
        
    }
}
