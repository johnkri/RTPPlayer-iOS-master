//
//  UIView+Extensions.swift
//  RTPPlayer
//
//  Created by John Kricorian on 21/02/2022.
//  Copyright © 2022 Smartbox. All rights reserved.
//

import UIKit

extension UIView {
    
    func takeScreenshot2() -> UIImage? {
        let bounds = self.bounds
        var screenshotImage: UIImage?
        guard let layer = UIApplication.shared.keyWindow?.layer else { return nil }
        let scale = UIScreen.main.scale
        UIGraphicsBeginImageContextWithOptions(bounds.size, false, scale)
        guard let context = UIGraphicsGetCurrentContext() else { return nil }
        layer.render(in: context)
        screenshotImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return screenshotImage
    }
    
    func takeScreenshot() -> UIImage? {
        
        UIGraphicsBeginImageContextWithOptions(self.bounds.size, false, UIScreen.main.scale)
        drawHierarchy(in: self.bounds, afterScreenUpdates: true)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        guard let image = image else { return nil }
        
        return image
        
    }
}

