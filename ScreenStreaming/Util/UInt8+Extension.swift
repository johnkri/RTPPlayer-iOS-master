//
//  UInt8+Extension.swift
//  RTPPlayer
//
//  Created by John Kricorian on 21/01/2022.
//  Copyright © 2022 Smartbox. All rights reserved.
//

import Foundation

extension Int {
    
    func toUInt8() -> UInt8 {
        let int8: UInt8? = withUnsafeBytes(of: self) { ptr -> UInt8? in
            let binded = ptr.bindMemory(to: UInt8.self)
            return binded.first
        }
        return int8 ?? UInt8(0)
    }
}
