//
//  CGFloat+Extension.swift
//  RTPPlayer
//
//  Created by John Kricorian on 04/02/2022.
//  Copyright © 2022 Smartbox. All rights reserved.
//

import UIKit

extension CGFloat {
    static func random() -> CGFloat {
        return CGFloat(arc4random()) / CGFloat(UInt32.max)
    }
}

