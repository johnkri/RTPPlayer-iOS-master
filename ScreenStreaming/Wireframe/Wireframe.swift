//
//  Wireframe.swift
//  RTPPlayer
//
//  Created by John Kricorian on 24/02/2022.
//  Copyright © 2022 Smartbox. All rights reserved.
//

import Foundation
import UIKit

protocol WireframeDelegate {
    func createMainModule() -> MainViewController
}

final class Wireframe {
    
    static let _sharedInstance = Wireframe()
    
    class func shared() -> Wireframe {
        return _sharedInstance
    }
        
    private init() {}
    
}

extension Wireframe: WireframeDelegate {
    
    func createMainModule() -> MainViewController {
        let mainView: MainViewDelegate = MainViewController()
        buildDependencies(for: mainView)
        
        return mainView as! MainViewController
    }
    
    func buildDependencies(for mainView: MainViewDelegate) {
        let customRecorder: CustomRecorderDelegate = CustomRecorder()
        let enqueuer: EnqueuerDelegate = Enqueuer()
        let jpegParser: JpegParserDelegate = JpegParser()
        let packetHeader: PacketHeaderDelegate = PacketHeader()
        let network: NetworkDelegate = Network()
        
        mainView.customRecorder = customRecorder
        mainView.enqueueur = enqueuer
        customRecorder.enqueuer = enqueuer
        enqueuer.packetHeader = packetHeader
        enqueuer.jpegParser = jpegParser
        packetHeader.network = network
    }
}

