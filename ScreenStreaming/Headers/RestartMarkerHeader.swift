//
//  RestartMarkerHeader.swift
//  RTPPlayer
//
//  Created by John Kricorian on 01/02/2022.
//  Copyright © 2022 Smartbox. All rights reserved.
//

import Foundation

class RestartMarkerHeader  {
    
    var restartInterval: [UInt8]
    var first_Last_RestartCount: [UInt8] = [0xFF, 0xFF]
    
    init(restartInterval: [UInt8]) {
        self.restartInterval = restartInterval
    }
    
    var header: [UInt8] {
        var output: [UInt8] = []
        output.append(contentsOf: restartInterval)
        output.append(contentsOf: first_Last_RestartCount)

        return output
    }
    
}

