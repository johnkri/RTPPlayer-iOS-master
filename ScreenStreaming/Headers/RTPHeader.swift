//
//  RTPHeader.swift
//  RTPPlayer
//
//  Created by John Kricorian on 21/01/2022.
//  Copyright © 2022 Smartbox. All rights reserved.
//

import Foundation


class RTPHeader {
    
    var marker: UInt8
    var sequenceNumber: Int
    var timeStamp: Int
    
    init(marker: UInt8, sequenceNumber: Int, timeStamp: Int) {
        self.marker = marker
        self.sequenceNumber = sequenceNumber
        self.timeStamp = timeStamp
    }
    
    /** RTP version, refer to RFC 1889 */
    var VERSION: UInt8 = 2
    
    /** Padding value. */
    var PADDING: UInt8 = 0
    
    /** Extension header. Not set (set to 0) so no extension header follow the header. */
    var EXTENSION: UInt8 = 0
    
    /** Contributing source identifier ? Not really used. */
    var CC: UInt8 = 0
    
    /** Payload type for JPEG. */
    var PAYLOAD_TYPE: UInt8 = 26
    /**
     * Synchronization Source identifier, not really used. Note that Contributing Source
     * identifier is not present.
     */
    var SYNC_SOURCE_ID = 21845 // Identifies the server
    
    var header: [UInt8] {
        // fill the header array of byte with RTP header fields
        var header: [UInt8] = []
        header.append(VERSION << 6 | PADDING << 5 | EXTENSION << 4 | CC)
        header.append(marker << 7 | PAYLOAD_TYPE & 0xFF)
        header.append((sequenceNumber >> 8).toUInt8())
        header.append((sequenceNumber & 0xFF).toUInt8())
        header.append((timeStamp >> 24).toUInt8())
        header.append((timeStamp >> 16).toUInt8())
        header.append((timeStamp >> 8).toUInt8())
        header.append((timeStamp).toUInt8())
        header.append((SYNC_SOURCE_ID >> 24).toUInt8())
        header.append((SYNC_SOURCE_ID >> 16).toUInt8())
        header.append((SYNC_SOURCE_ID >> 8).toUInt8())
        header.append((SYNC_SOURCE_ID & 0xFF).toUInt8())
        return header
    }
}


