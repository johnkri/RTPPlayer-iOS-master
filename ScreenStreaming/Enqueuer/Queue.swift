//
//  Queue.swift
//  RTPPlayer
//
//  Created by John Kricorian on 09/02/2022.
//  Copyright © 2022 Smartbox. All rights reserved.
//

import Foundation

struct Queue<T> {
  private var elements: [T] = []

  mutating func enqueue(_ value: T) {
    elements.append(value)
  }

  mutating func dequeue() -> T? {
    guard !elements.isEmpty else {
      return nil
    }
    return elements.removeFirst()
  }

  var head: T? {
    return elements.first
  }

  var tail: T? {
    return elements.last
  }
}
