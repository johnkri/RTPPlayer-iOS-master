//
//  Enqueuer.swift
//  RTPPlayer
//
//  Created by John Kricorian on 18/02/2022.
//  Copyright © 2022 Smartbox. All rights reserved.
//

import ReplayKit
import UIKit

protocol EnqueuerDelegate: AnyObject {
    var jpegParser: JpegParserDelegate? { get set }
    var packetHeader: PacketHeaderDelegate? { get set }
    var screenshot: UIImage? { get set }
    var sampleBuffer: CMSampleBuffer? { get set }
}

class Enqueuer: EnqueuerDelegate {
    
    internal var packetHeader: PacketHeaderDelegate?
    internal var jpegParser: JpegParserDelegate?
    internal var screenshot: UIImage?
    internal var sampleBuffer: CMSampleBuffer?
    
    private var imageQueue = Queue<UIImage>()
    private var sampleQueue = Queue<CMSampleBuffer>()

    internal init() {
//        updateSampleQueue()
        updateScreenshotQueue()
    }
    
    private func parseJpegData(_ sampleBuffer: CMSampleBuffer) {
        jpegParser?.parse(sampleBuffer) { rtpPacket in
            let dispatchQueue = DispatchQueue(label: "QueueIdentification", qos: .background)
            dispatchQueue.async {
                self.packetHeader?.createHeader(rtpPacket: rtpPacket)
            }
        }
    }
    
    private func parseJpegData(_ screenshot: UIImage) {
        jpegParser?.parse(screenshot) { rtpPacket in
            let dispatchQueue = DispatchQueue(label: "QueueIdentification", qos: .background)
            dispatchQueue.async {
                self.packetHeader?.createHeader(rtpPacket: rtpPacket)
            }
        }
    }
    
    private func updateSampleQueue() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0) {
            if let sampleBuffer = self.sampleBuffer {
                self.sampleQueue.enqueue(sampleBuffer)
                if let sampleBuffer = self.sampleQueue.head {
                    self.parseJpegData(sampleBuffer)
                    _ = self.sampleQueue.dequeue()
                }
            }
            self.updateSampleQueue()
        }
    }
    
    private func updateScreenshotQueue() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0) {
            if let screenshot = self.screenshot {
                self.imageQueue.enqueue(screenshot)
                if let screenshot = self.imageQueue.head {
                    self.parseJpegData(screenshot)
                    _ = self.imageQueue.dequeue()
                }
            }
            self.updateScreenshotQueue()
        }
    }
}

