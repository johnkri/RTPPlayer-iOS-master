//
//  VideoFrame.swift
//  ScreenStreaming
//
//  Created by John Kricorian on 28/02/2022.
//

import CoreMedia
import QuartzCore
import AssetsLibrary

class VideoFrame {

    var outputBufferPool: CVPixelBufferPool
    var pixelBuffer: CVPixelBuffer
    var rgbColorSpace: CGColorSpace
    var viewSize: CGSize
    var scale: CGFloat
    
    let frameRenderingSemaphore: DispatchSemaphore?
    
    internal init(outputBufferPool: CVPixelBufferPool, pixelBuffer: CVPixelBuffer, rgbColorSpace: CGColorSpace, viewSize: CGSize, scale: CGFloat) {
        self.outputBufferPool = outputBufferPool
        self.pixelBuffer = pixelBuffer
        self.rgbColorSpace = rgbColorSpace
        self.viewSize = viewSize
        self.scale = scale
    }

    func writeVideoFrame() {
        if (frameRenderingSemaphore?.wait(timeout: DispatchTime.now()) == .success ? 0 : -1) != 0 {
            return
        }
    }

    func createPixelBufferAndBitmapContext(_ pixelBuffer: CVPixelBuffer?) -> CGContext? {
        var pixelBuffer = pixelBuffer
        CVPixelBufferPoolCreatePixelBuffer(nil, outputBufferPool, &pixelBuffer)
        if let pixelBuffer = pixelBuffer {
            CVPixelBufferLockBaseAddress(pixelBuffer, [])
        }

        var bitmapContext: CGContext? = nil
        if let pixelBuffer = pixelBuffer {
            bitmapContext = CGContext(
                data: CVPixelBufferGetBaseAddress(pixelBuffer),
                width: CVPixelBufferGetWidth(pixelBuffer),
                height: CVPixelBufferGetHeight(pixelBuffer),
                bitsPerComponent: 8,
                bytesPerRow: CVPixelBufferGetBytesPerRow(pixelBuffer),
                space: rgbColorSpace,
                bitmapInfo: CGBitmapInfo.byteOrder32Little.rawValue | CGImageAlphaInfo.premultipliedFirst.rawValue)
        }
        bitmapContext?.scaleBy(x: scale, y: scale)
        let flipVertical = CGAffineTransform(a: 1, b: 0, c: 0, d: -1, tx: 0, ty: viewSize.height)
        bitmapContext?.concatenate(flipVertical)

        return bitmapContext
    }
}
