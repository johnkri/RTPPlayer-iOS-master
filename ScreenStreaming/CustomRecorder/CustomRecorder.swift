//
//  Screen.swift
//  RTPPlayer
//
//  Created by John Kricorian on 24/02/2022.
//  Copyright © 2022 Smartbox. All rights reserved.
//

import UIKit

protocol CustomRecorderDelegate: AnyObject {
    var enqueuer: EnqueuerDelegate? { get set }
    func startRecord()
}

class CustomRecorder: CustomRecorderDelegate {

    internal var enqueuer: EnqueuerDelegate?
    
    internal func startRecord() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
            if let rootViewController = UIApplication.shared.keyWindow?.rootViewController {
                if let view = rootViewController.view,
                   let screenshot = view.takeScreenshot() {
                    self.enqueuer?.screenshot = screenshot
                }
            }
            self.startRecord()
        }
    }
}
